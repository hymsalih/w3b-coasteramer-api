<?php
require_once "db/AppManager.php";
$db = AppManager::getPM();
set_time_limit(0);
$url = "api/product/GetGroupList";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api.coasteramer.com/" . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 1000,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "keycode: ".key_code,
    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if (!$err) {
    file_put_contents("source/GetGroupList.json", $response);
    $response = file_get_contents("source/GetGroupList.json");
    $group_lists = json_decode($response);
    foreach ($group_lists as $group_list) {
        $checkGroupList = $db->fetchResult("SELECT * FROM group_lists WHERE group_number='" . $group_list->GroupNumber . "' LIMIT 1");
        if(empty($checkGroupList)){
            $sql = "INSERT INTO `group_lists` (`id`, `main_product_number`, `group_number`) VALUES (null, '" . (!empty($group_list->MainProductNumber)?$group_list->MainProductNumber:'') . "','" .  $group_list->GroupNumber . "')";
            $db->executeQuery($sql);
        }else{
            $sql = "UPDATE  `group_lists` SET `main_product_number`='".(!empty($group_list->MainProductNumber)?$group_list->MainProductNumber:'')."', `group_number`='".$group_list->GroupNumber."' WHERE `id`='".$checkGroupList[0]['id']."'";
            $db->executeQuery($sql);
        }
    }

}




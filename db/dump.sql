-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2019 at 08:43 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `work_ebay_mark_chin`
--

-- --------------------------------------------------------

--
-- Table structure for table `additional_field_lists`
--

CREATE TABLE `additional_field_lists` (
  `id` int(11) NOT NULL,
  `additional_field_lists_details` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_code` varchar(10) NOT NULL,
  `category_name` varchar(34) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `collection_lists`
--

CREATE TABLE `collection_lists` (
  `id` int(11) NOT NULL,
  `collection_code` varchar(10) NOT NULL,
  `collection_name` varchar(30) NOT NULL,
  `image_url` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_lists`
--

CREATE TABLE `group_lists` (
  `id` int(11) NOT NULL,
  `main_product_number` varchar(10) DEFAULT NULL,
  `group_number` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_lists`
--

CREATE TABLE `inventory_lists` (
  `id` int(11) NOT NULL,
  `warehouse` int(11) NOT NULL,
  `product_number` int(11) NOT NULL,
  `quantity_available` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `material_lists`
--

CREATE TABLE `material_lists` (
  `id` int(11) NOT NULL,
  `material_lists_details` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `measurements`
--

CREATE TABLE `measurements` (
  `id` int(11) NOT NULL,
  `measurement_details` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `new_product_list`
--

CREATE TABLE `new_product_list` (
  `id` int(1) NOT NULL,
  `product_number` varchar(30) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext,
  `measurement_list` longtext,
  `material_list` longtext,
  `additional_field_list` longtext,
  `components` longtext,
  `style_code` varchar(15) DEFAULT NULL,
  `collection_code` varchar(15) DEFAULT NULL,
  `product_line_code` varchar(15) NOT NULL,
  `finish_color` varchar(15) DEFAULT NULL,
  `box_weight` float NOT NULL,
  `cubes` double NOT NULL,
  `relational_product_lists` int(1) DEFAULT NULL,
  `type_of_packaging` varchar(10) DEFAULT NULL,
  `box_size` varchar(128) NOT NULL,
  `upc` varchar(24) DEFAULT NULL,
  `category_code` varchar(10) DEFAULT NULL,
  `sub_category_code` varchar(10) DEFAULT NULL,
  `piece_code` varchar(10) DEFAULT NULL,
  `category_root` longtext,
  `country_of_orgin` varchar(50) DEFAULT NULL,
  `design_collection` varchar(30) DEFAULT NULL,
  `assembly_required` int(1) DEFAULT NULL,
  `is_discountinued` int(1) DEFAULT NULL,
  `num_images` tinyint(1) NOT NULL DEFAULT '0',
  `num_boxes` tinyint(1) NOT NULL DEFAULT '0',
  `pack_qty` tinyint(1) DEFAULT '0',
  `catalog_page` int(1) NOT NULL DEFAULT '0',
  `num_hd_images` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_processed` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT '0' COMMENT 'If 1 then Amazon processed. If 0 them Amazon did not process - most likely missing UPC',
  `asin` varchar(34) DEFAULT NULL COMMENT 'If NULL (and Amazon_processed = 1) after running  Coaster script GetProductList.php then product not in Amazon - create template',
  `amazon_listed` tinyint(1) DEFAULT '0',
  `unit_cost` decimal(10,2) DEFAULT '0.00',
  `display_height` float DEFAULT '0',
  `display_width` float DEFAULT '0',
  `display_length` float DEFAULT '0',
  `display_weight` float DEFAULT '0',
  `image_url` varchar(255) DEFAULT NULL,
  `summary` longtext,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `piece_lists`
--

CREATE TABLE `piece_lists` (
  `id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `piece_code` varchar(10) NOT NULL,
  `piece_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `price_exception_lists`
--

CREATE TABLE `price_exception_lists` (
  `id` int(11) NOT NULL,
  `price_exception_code` varchar(34) NOT NULL,
  `product_number` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `price_lists`
--

CREATE TABLE `price_lists` (
  `id` int(11) NOT NULL,
  `price_code` varchar(10) NOT NULL,
  `product_number` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(1) NOT NULL,
  `product_number` varchar(30) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext,
  `measurement_list` longtext,
  `material_list` longtext,
  `additional_field_list` longtext,
  `style_code` varchar(15) DEFAULT NULL,
  `collection_code` varchar(15) DEFAULT NULL,
  `product_line_code` varchar(15) NOT NULL,
  `finish_color` varchar(15) DEFAULT NULL,
  `box_weight` double NOT NULL,
  `cubes` double NOT NULL,
  `relational_product_lists` int(1) DEFAULT NULL,
  `type_of_packaging` varchar(10) DEFAULT NULL,
  `box_size` varchar(128) NOT NULL,
  `upc` varchar(13) DEFAULT NULL,
  `category_code` varchar(10) DEFAULT NULL,
  `sub_category_code` varchar(10) DEFAULT NULL,
  `piece_code` varchar(10) DEFAULT NULL,
  `country_of_orgin` varchar(3) DEFAULT NULL,
  `design_collection` varchar(30) DEFAULT NULL,
  `assembly_required` int(1) DEFAULT NULL,
  `is_discountinued` int(1) DEFAULT NULL,
  `num_images` tinyint(1) NOT NULL DEFAULT '0',
  `num_boxes` tinyint(1) NOT NULL DEFAULT '0',
  `pack_qty` tinyint(1) DEFAULT '0',
  `catalog_page` int(1) NOT NULL DEFAULT '0',
  `num_hd_images` tinyint(1) NOT NULL DEFAULT '0',
  `amazon_processed` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT '0' COMMENT 'If 1 then Amazon processed. If 0 them Amazon did not process - most likely missing UPC',
  `asin` varchar(34) DEFAULT NULL COMMENT 'If NULL (and Amazon_processed = 1) after running  Coaster script GetProductList.php then product not in Amazon - create template',
  `amazon_listed` int(1) DEFAULT '0',
  `unit_cost` decimal(10,2) DEFAULT NULL,
  `display_height` smallint(1) DEFAULT NULL,
  `display_width` smallint(1) DEFAULT NULL,
  `display_length` smallint(1) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `related_product_lists`
--

CREATE TABLE `related_product_lists` (
  `id` int(11) NOT NULL,
  `related_product_lists_details` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `style_lists`
--

CREATE TABLE `style_lists` (
  `id` int(11) NOT NULL,
  `style_code` varchar(10) NOT NULL,
  `style_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `parent_category` int(11) NOT NULL,
  `sub_category_code` varchar(10) NOT NULL,
  `sub_category_name` varchar(34) NOT NULL,
  `amazon_node_name` varchar(255) DEFAULT NULL,
  `amazon_node_id` bigint(20) NOT NULL,
  `amazon_node_path` longtext,
  `amazon_inventory_template_name` varchar(255) DEFAULT NULL,
  `amazon_item_type_keyword` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `variations_products`
--

CREATE TABLE `variations_products` (
  `id` int(11) NOT NULL,
  `variation_of` int(11) NOT NULL,
  `product_number` varchar(30) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `box_sizes` varchar(255) DEFAULT NULL,
  `box_weight` double DEFAULT '0',
  `cubes` double NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ware_house`
--

CREATE TABLE `ware_house` (
  `id` int(11) NOT NULL,
  `ware_house_code` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additional_field_lists`
--
ALTER TABLE `additional_field_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_code` (`category_code`);

--
-- Indexes for table `collection_lists`
--
ALTER TABLE `collection_lists`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `collection_code` (`collection_code`);

--
-- Indexes for table `group_lists`
--
ALTER TABLE `group_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_lists`
--
ALTER TABLE `inventory_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_lists`
--
ALTER TABLE `material_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `measurements`
--
ALTER TABLE `measurements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_product_list`
--
ALTER TABLE `new_product_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_number` (`product_number`);

--
-- Indexes for table `piece_lists`
--
ALTER TABLE `piece_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_exception_lists`
--
ALTER TABLE `price_exception_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_lists`
--
ALTER TABLE `price_lists`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_number` (`product_number`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_number` (`product_number`);

--
-- Indexes for table `related_product_lists`
--
ALTER TABLE `related_product_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `style_lists`
--
ALTER TABLE `style_lists`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `style_code` (`style_code`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variations_products`
--
ALTER TABLE `variations_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_number` (`product_number`);

--
-- Indexes for table `ware_house`
--
ALTER TABLE `ware_house`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additional_field_lists`
--
ALTER TABLE `additional_field_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `collection_lists`
--
ALTER TABLE `collection_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_lists`
--
ALTER TABLE `group_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventory_lists`
--
ALTER TABLE `inventory_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `material_lists`
--
ALTER TABLE `material_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `measurements`
--
ALTER TABLE `measurements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `new_product_list`
--
ALTER TABLE `new_product_list`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `piece_lists`
--
ALTER TABLE `piece_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `price_exception_lists`
--
ALTER TABLE `price_exception_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `price_lists`
--
ALTER TABLE `price_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `related_product_lists`
--
ALTER TABLE `related_product_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `style_lists`
--
ALTER TABLE `style_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `variations_products`
--
ALTER TABLE `variations_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ware_house`
--
ALTER TABLE `ware_house`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

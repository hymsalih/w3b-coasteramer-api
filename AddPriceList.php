<?php
require_once "db/AppManager.php";
$db = AppManager::getPM();
set_time_limit(0);
$url = "api/product/GetPriceList";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api.coasteramer.com/" . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 1000,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "keycode: ".key_code,
    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if (!$err) {
    file_put_contents("source/GetPriceList.json", $response);
    $response = file_get_contents("source/GetPriceList.json");
$price_lists = json_decode($response);
foreach ($price_lists as $price_list) {
    $price_code = $price_list->PriceCode;
    foreach($price_list->PriceList as $price){
        if(!empty($price->ProductNumber)){
            $checkItem = $db->fetchResult("SELECT * FROM products WHERE product_number='" . $price->ProductNumber . "' LIMIT 1");
            if(!empty($checkItem)){
                $sql = "SELECT * FROM  `price_lists` WHERE  `product_number` = '". $checkItem[0]['id'] ."' AND  `price_code` = '".$price_code."'";
                $checkPrice = $db->fetchResult($sql);
                if(empty($checkPrice)){
                    $sql = "INSERT INTO `price_lists` (`id`, `price_code`, `product_number`, `price`) VALUES (NULL, '".$price_code."', '". $checkItem[0]['id']."', '".$price->Price."')";
                    $db->executeQuery($sql);
                }else{
                    $sql = "UPDATE  `price_lists`  SET  `price`='".$price->Price."' WHERE id='".$checkPrice[0]['id']."'";
                    $db->executeQuery($sql);
                    $sql = "UPDATE  `products`  SET  `price`='".$price->Price."' WHERE id='".$checkPrice[0]['id']."'";
                    $db->executeQuery($sql);
                }
        
            }

        }
}
}
}

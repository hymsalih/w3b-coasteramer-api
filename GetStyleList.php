<?php
require_once "db/AppManager.php";
$db = AppManager::getPM();
set_time_limit(0);
$url = "api/product/GetStyleList";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api.coasteramer.com/" . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 1000,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "keycode: ".key_code,
    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if (!$err) {
    file_put_contents("source/GetStyleList.json", $response);
$response = file_get_contents("source/GetStyleList.json");
$style_lists = json_decode($response);
foreach ($style_lists as $style_list) {
$checkStyle = $db->fetchResult("SELECT * FROM style_lists WHERE style_code='" . $style_list->StyleCode . "' LIMIT 1");
if(empty($checkStyle)){
    $sql = "INSERT INTO `style_lists` (`id`, `style_code`, `style_name`) VALUES (null, '" . addslashes($style_list->StyleCode) . "','" .  addslashes($style_list->StyleName) . "')";
    $db->executeQuery($sql);
}else{
    $sql = "UPDATE  `style_lists` SET `style_code`='".addslashes($style_list->StyleCode)."', `style_name`='".addslashes($style_list->StyleName)."' WHERE `id`='".$checkStyle[0]['id']."'";
    $db->executeQuery($sql);
}
}

}

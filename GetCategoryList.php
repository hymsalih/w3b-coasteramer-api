<?php

require_once "db/AppManager.php";
$db = AppManager::getPM();
set_time_limit(0);
$url = "api/product/GetCategoryList";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api.coasteramer.com/" . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 1000,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "keycode: ".key_code,
    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if (!$err) {
    file_put_contents("source/GetCategoryList.json", $response);
$response = file_get_contents("source/GetCategoryList.json");
$category_lists = json_decode($response);
foreach ($category_lists as $category_list) {
    $category_name =  $category_list->CategoryName;
    $category_code =  $category_list->CategoryCode;
    $checkCategory = $db->getCount("SELECT count(*) c FROM category WHERE category_code='" . $category_code . "'");
    if($checkCategory ==0 ){
        $sql = "INSERT INTO `category` (`id`, `category_code`, `category_name`) VALUES (null, '" . addslashes($category_code) . "','" . addslashes($category_name) . "')";
        $db->executeQuery($sql);
       $sql = "SELECT id FROM `category` ORDER BY id DESC";
       $category_id = $db->fetchResult($sql);
       $category_id = $category_id[0]['id'];
       foreach($category_list->SubCategoryList as $sub_category_lists){
        $sql = "INSERT INTO `sub_category` (`id`, `parent_category`, `sub_category_code`, `sub_category_name`) VALUES (null,'".$category_id."', '" . $sub_category_lists->SubCategoryCode. "','" . addslashes($sub_category_lists->SubCategoryName) . "')";
        $db->executeQuery($sql);
        $sql = "SELECT id FROM `sub_category` ORDER BY id DESC";
        $sub_category_id = $db->fetchResult($sql);
        $sub_category_id = $sub_category_id[0]['id'];
        foreach($sub_category_lists->PieceList as $sub_category_piece_lists){
            $sql = "INSERT INTO `piece_lists` (`id`, `sub_category_id`, `piece_code`, `piece_name`) VALUES (null,'".$sub_category_id."', '" . $sub_category_piece_lists->PieceCode. "','" . addslashes($sub_category_piece_lists->PieceName) . "')";
            $db->executeQuery($sql);

        }
    }
    }

}
}

<?php
require_once "db/AppManager.php";
$db = AppManager::getPM();
set_time_limit(0);
$url = "api/product/GetCollectionList";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api.coasteramer.com/" . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 1000,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "keycode: ".key_code,
    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if (!$err) {
    file_put_contents("source/GetCollectionList.json", $response);
$response = file_get_contents("source/GetCollectionList.json");
$collection_lists = json_decode($response);
foreach ($collection_lists as $collection_list) {
$checkCollection = $db->fetchResult("SELECT * FROM collection_lists WHERE collection_code='" . $collection_list->CollectionCode . "' LIMIT 1");
if(empty($checkCollection)){
    $sql = "INSERT INTO `collection_lists` (`id`, `collection_code`, `collection_name`, `image_url`) VALUES (null, '" . addslashes($collection_list->CollectionCode) . "','" .  addslashes($collection_list->CollectionName) . "','" .  (!empty($collection_list->ImageUrl)?$collection_list->ImageUrl:'') . "')";
    $db->executeQuery($sql);
}else{
    $sql = "UPDATE  `collection_lists` SET `collection_code`='".addslashes($collection_list->CollectionCode)."', `collection_name`='".addslashes($collection_list->CollectionName)."', `image_url`='".(!empty($collection_list->ImageUrl)?$collection_list->ImageUrl:'')."' WHERE `id`='".$checkCollection[0]['id']."'";
    $db->executeQuery($sql);
}
}

}

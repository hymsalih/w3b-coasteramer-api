<?php


error_reporting(E_ALL);
ini_set("memory_limit", "-1");
require_once "db/AppManager.php";
$db = AppManager::getPM();
set_time_limit(0);
$url = "api/product/GetProductList";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api.coasteramer.com/" . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 1000,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "keycode: " . key_code,
    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if (!$err) {
    $valid_response = json_validator($response);
    if (empty($valid_response)) {
        echo "\n\nerror";
        die;
    }
    $product_lists = json_decode($response);
    foreach ($product_lists as $product_list) {
        $summary = (!empty($product_list->Description) ? $product_list->Description . "<br>" : "");
        $summary .= "COLOR: " . (!empty($product_list->FinishColor) ? $product_list->FinishColor : "N/A") . "<br>";
        $summary .= "HEIGHT: " . (!empty($product_list->BoxSize->Height) ? $product_list->BoxSize->Height : "N/A") . "<br>";
        $summary .= "LENGTH: " . (!empty($product_list->BoxSize->Length) ? $product_list->BoxSize->Length : "N/A") . "<br>";
        $summary .= "WIDTH: " . (!empty($product_list->BoxSize->Width) ? $product_list->BoxSize->Width : "N/A") . "<br>";
        $summary .= "WEIGHT: " . (!empty($product_list->BoxWeight) ? $product_list->BoxWeight : "N/A") . "<br>";
        $summary .= "CATALOG PAGE: " . (!empty($product_list->CatalogPage) ? $product_list->CatalogPage : "N/A") . "<br>";
        $checkProduct = $db->fetchResult("SELECT *  FROM  new_product_list WHERE product_number='" . $product_list->ProductNumber . "'");
        $image_url = array();
        if ($product_list->NumHDImages == 0) {
            for ($x = 1; $x <= $product_list->NumImages; $x++) {
                $image_url[] = "http://assets.coasteramer.com/productpictures/" . $product_list->ProductNumber . "/$x.jpg?width=900&height=900";
            }
        } else {
            for ($x = 1; $x <= $product_list->NumImages; $x++) {
                $image_url[] = "http://assets.coasteramer.com/productpictures/" . $product_list->ProductNumber . "/$x.jpg";
            }
        }
        if (count($checkProduct) == 0) {
            echo $product_list->ProductNumber . "\n";
            //insert measurements
            if (!empty($product_list->MeasurementList)) {
                $measurement_list = json_encode($product_list->MeasurementList);
            } else {
                $measurement_list = json_encode(array());
            }
            if (!empty($product_list->MaterialList)) {
                $material_lists = json_encode($product_list->MaterialList);
            } else {
                $material_lists = json_encode(array());
            }
            //insert additional_field_lists
            if (!empty($product_list->AdditionalFieldList)) {
                $addition_field_lists = json_encode($product_list->AdditionalFieldList);
            } else {
                $addition_field_lists = json_encode(array());
            }
            //insert related_product_lists

            if (!empty($product_list->RelatedProductList)) {
                $related_product_lists = json_encode($product_list->RelatedProductList);
            } else {
                $related_product_lists = json_encode(array());
            }
            if (isset($product_list->Components)) {
                $components = json_encode($product_list->Components);
            } else {
                $components = json_encode(array());
            }
            $category_root = "";
            if (!empty($product_list->CategoryCode)) {
                $sql = "SELECT * FROM `category` WHERE category_code = '" . $product_list->CategoryCode . "' LIMIT 1";
                $category = $db->fetchResult($sql);
                $category_root = $category[0]['category_name'];
            }

            if (!empty($product_list->SubcategoryCode)) {
                $sql = "SELECT * FROM `sub_category` WHERE sub_category_code = '" . $product_list->SubcategoryCode . "' LIMIT 1";
                $category = $db->fetchResult($sql);
                $category_root .= " | " . (!empty($category) ? $category[0]['sub_category_name'] : "");
            }

            if (!empty($product_list->PieceCode)) {
                $sql = "SELECT * FROM `piece_lists` WHERE piece_code = '" . $product_list->PieceCode . "' LIMIT 1";
                $category = $db->fetchResult($sql);
                $category_root .= " | " . (!empty($category) ? $category[0]['piece_name'] : "");
            }
            $sql = "INSERT INTO `new_product_list` (`id`, `image_url`, `category_root`, `product_number`, `name`, `description`, `measurement_list`,
         `material_list`, `additional_field_list`, `style_code`, `collection_code`, `product_line_code`, `finish_color`, `box_weight`, `cubes`,
          `relational_product_lists`, `type_of_packaging`,`box_size`, `upc`, `category_code`, `sub_category_code`, `piece_code`, `country_of_orgin`,
           `design_collection`, `assembly_required`, `is_discountinued`, `num_images`, `num_boxes`, `pack_qty`, `catalog_page`, `num_hd_images` ,`components` ,`summary`,`length`,`width`,`height`) 
                                VALUES (NULL, '" . json_encode($image_url) . "','" . addslashes($category_root) . "', '" . addslashes($product_list->ProductNumber) . "', '" . addslashes($product_list->Name) . "', '" . (!empty($product_list->Description) ? addslashes($product_list->Description) : '') . "', 
                                '" . addslashes($measurement_list) . "', '" . addslashes($material_lists) . "', '" . addslashes($addition_field_lists) . "', '" . (!empty($product_list->StyleCode) ? addslashes($product_list->StyleCode) : '') . "', 
                                '" . (!empty($product_list->CollectionCode) ? addslashes($product_list->CollectionCode) : '') . "', '" . (!empty($product_list->ProductLineCode) ? $product_list->ProductLineCode : '') . "', '" . (!empty($product_list->FinishColor) ? addslashes($product_list->FinishColor) : '') . "',
                                 '" . $product_list->BoxWeight . "', '" . addslashes($product_list->Cubes) . "', '" . addslashes($related_product_lists) . "',
                                  '" . (!empty($product_list->TypeOfPackaging) ? addslashes($product_list->TypeOfPackaging) : '') . "','" . json_encode($product_list->BoxSize) . "','" . (!empty($product_list->Upc) ? addslashes($product_list->Upc) : '') . "',
                                  '" . (!empty($product_list->CategoryCode) ? addslashes($product_list->CategoryCode) : '') . "', '" . (!empty($product_list->SubcategoryCode) ? addslashes($product_list->SubcategoryCode) : '') . "', 
                                   '" . (!empty($product_list->PieceCode) ? addslashes($product_list->PieceCode) : '') . "', '" . (!empty($product_list->CountryOfOrigin) ? addslashes($product_list->CountryOfOrigin) : '') . "', 
                                   '" . addslashes($product_list->DesignerCollection) . "', '" . (!empty($product_list->AssemblyRequired) ? $product_list->AssemblyRequired : 0) . "', 
                                   '" . (!empty(addslashes($product_list->IsDiscontinued)) ? addslashes($product_list->IsDiscontinued) : 0) . "', '" . addslashes($product_list->NumImages) . "', '" . $product_list->NumBoxes . "', 
                                   '" . addslashes($product_list->PackQty) . "', '" . (!empty($product_list->CatalogPage) ? addslashes($product_list->CatalogPage) : 0) . "', '" . addslashes($product_list->NumHDImages) . "', '" . addslashes($components) . "', '" . addslashes($summary) . "', '" . $product_list->BoxSize->Length . "', '" . $product_list->BoxSize->Width . "', '" . $product_list->BoxSize->Height . "')";
            $db->executeQuery($sql);
        } else {
            $updated_at = gmdate("y-m-d h:i:s");
//            $sql = "UPDATE  `new_product_list` SET `summary` = '" . addslashes($summary) . "',  `is_discountinued` = '" . (!empty(addslashes($product_list->IsDiscontinued)) ? addslashes($product_list->IsDiscontinued) : 0) . "', `updated_at` = '" . $updated_at . "'  WHERE id='" . $checkProduct[0]['id'] . "'";
            $sql = "UPDATE  `new_product_list` SET  `is_discountinued` = '" . (!empty(addslashes($product_list->IsDiscontinued)) ? addslashes($product_list->IsDiscontinued) : 0) . "', `updated_at` = '" . $updated_at . "'  WHERE id='" . $checkProduct[0]['id'] . "'";
            $db->executeQuery($sql);
        }
    }
}
die;

function json_validator($data = NULL)
{
    if (!empty($data)) {
        @json_decode($data);
        return (json_last_error() === JSON_ERROR_NONE);
    }
    return false;
}

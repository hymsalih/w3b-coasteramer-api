<?php
require_once "db/AppManager.php";
$db = AppManager::getPM();
set_time_limit(0);
$url = "api/product/GetInventoryList";
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api.coasteramer.com/" . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 1000,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "keycode: " . key_code,

    ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if (!$err) {
    $inventory_lists = json_decode($response);
    foreach ($inventory_lists as $inventory_list) {
        foreach ($inventory_list->InventoryList as $inventory) {
            if (!empty($inventory->ProductNumber)) {
                $updated_at = gmdate("y-m-d h:i:s");
                $sql = "UPDATE  `new_product_list`  SET `updated_at` = '" . $updated_at . "',  `quantity`='" . $inventory->QtyAvail . "' WHERE product_number='" . $inventory->ProductNumber . "'";
                $db->executeQuery($sql);
            }
        }

    }

}

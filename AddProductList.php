<?php

require_once "db/AppManager.php";
$db = AppManager::getPM();
set_time_limit(0);

$url = "api/product/GetProductList";

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api.coasteramer.com/" . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 1000,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "keycode: ".key_code,
        
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);


if (!$err) {
   file_put_contents("source/GetProductList.json", $response);
    $response = file_get_contents("source/GetProductList.json");
$product_lists = json_decode($response);
foreach ($product_lists as $product_list) {
    $checkProduct = $db->getCount("SELECT count(*) c FROM  products WHERE product_number='" . $product_list->ProductNumber . "'");
    $checkVariationProduct = $db->getCount("SELECT count(*) c FROM  variations_products WHERE product_number='" . $product_list->ProductNumber . "'");
    if ($checkProduct == 0 && $checkVariationProduct == 0) {
        echo $product_list->ProductNumber . "\n";
        //insert measurements
        if(!empty($product_list->MeasurementList)){
            $measurement_list = json_encode($product_list->MeasurementList);
        }else{
            $measurement_list = json_encode(array());
        }
        $sql = "INSERT INTO `measurements` (`id`, `measurement_details`) VALUES (null, '" . addslashes($measurement_list) . "')";
        $db->executeQuery($sql);
        //get meaurement id
        $sql = "SELECT id FROM measurements ORDER BY id DESC LIMIT 1";
        $mesuremtnt_id = $db->fetchResult($sql);
        $mesuremtnt_id = $mesuremtnt_id[0]['id'];
        //insert material_lists
        if(!empty($product_list->MaterialList)){
            $material_lists = json_encode($product_list->MaterialList);
        }else{
            $material_lists = json_encode(array());
        }
        $sql = "INSERT INTO `material_lists` (`id`, `material_lists_details`) VALUES (null, '" . addslashes($material_lists) . "')";
        $db->executeQuery($sql);
        //get material_lists id
        $sql = "SELECT id FROM material_lists ORDER BY id DESC LIMIT 1";
        $material_lists_id = $db->fetchResult($sql);
        $material_lists_id = $material_lists_id[0]['id'];
        //insert additional_field_lists

        if(!empty($product_list->AdditionalFieldList)){
            $addition_field_lists = json_encode($product_list->AdditionalFieldList);
        }else{
            $addition_field_lists = json_encode(array());
        }

        $sql = "INSERT INTO `additional_field_lists` (`id`, `additional_field_lists_details`) VALUES (null, '" . addslashes($addition_field_lists) . "')";
        $db->executeQuery($sql);
        //get additional_field_lists id
        $sql = "SELECT id FROM additional_field_lists ORDER BY id DESC LIMIT 1";
        $additional_field_lists_id = $db->fetchResult($sql);
        $additional_field_lists_id = $additional_field_lists_id[0]['id'];
        //insert related_product_lists

        if(!empty($product_list->RelatedProductList)){
            $related_product_lists = json_encode($product_list->RelatedProductList);
        }else{
            $related_product_lists = json_encode(array());
        }
        $sql = "INSERT INTO `related_product_lists` (`id`, `related_product_lists_details`) VALUES (null, '" . $related_product_lists . "')";
        $db->executeQuery($sql);
        //get related_product_lists id
        $sql = "SELECT id FROM related_product_lists ORDER BY id DESC LIMIT 1";
        $related_product_lists_id = $db->fetchResult($sql);
        $related_product_lists_id = $related_product_lists_id[0]['id'];
        //insert product details
        $sql = "INSERT INTO `products` (`id`, `product_number`, `name`, `description`, `mesurement_list_id`,
         `material_list_id`, `additional_field_list_id`, `style_code`, `collection_code`, `product_line_code`, `finish_color`, `box_weight`, `cubes`,
          `relational_product_lists`, `type_of_packaging`,`box_size`, `upc`, `category_code`, `sub_category_code`, `piece_code`, `country_of_orgin`,
           `design_collection`, `assembly_required`, `is_discountinued`, `num_images`, `num_boxes`, `pack_qty`, `catalog_page`, `num_hd_images`) 
                                VALUES (NULL, '" . $product_list->ProductNumber . "', '" . addslashes($product_list->Name) . "', '" . (!empty($product_list->Description) ? addslashes($product_list->Description) :'') . "', 
                                '" . $mesuremtnt_id . "', '" . $material_lists_id . "', '" . $additional_field_lists_id . "', '" . (!empty($product_list->StyleCode)?$product_list->StyleCode :''). "', 
                                '" . (!empty($product_list->CollectionCode)?$product_list->CollectionCode:'') . "', '" .(!empty( $product_list->ProductLineCode)? $product_list->ProductLineCode:'') . "', '" . (!empty($product_list->FinishColor)?$product_list->FinishColor:'') . "',
                                 '" . $product_list->BoxWeight . "', '" . $product_list->Cubes . "', '" . $related_product_lists_id . "',
                                  '" . (!empty($product_list->TypeOfPackaging)?$product_list->TypeOfPackaging:'') . "','" . json_encode($product_list->BoxSize) . "','" . (!empty($product_list->Upc)?$product_list->Upc:'') . "',
                                  '" . (!empty( $product_list->CategoryCode)? $product_list->CategoryCode:'') . "', '" . (!empty($product_list->SubcategoryCode)?$product_list->SubcategoryCode:'') . "', 
                                   '" .(!empty( $product_list->PieceCode)? $product_list->PieceCode:'') . "', '" . (!empty($product_list->CountryOfOrigin)?$product_list->CountryOfOrigin:'') . "', 
                                   '" . $product_list->DesignerCollection . "', '" . $product_list->AssemblyRequired . "', 
                                   '" . $product_list->IsDiscontinued . "', '" . $product_list->NumImages . "', '" . $product_list->NumBoxes . "', 
                                   '" . $product_list->PackQty . "', '" . (!empty($product_list->CatalogPage)?$product_list->CatalogPage :0). "', '" . $product_list->NumHDImages . "')";
        $db->executeQuery($sql);
        //variation products
        if(isset($product_list->Components)){
            $sql = "SELECT id FROM `products` WHERE product_number = '".$product_list->ProductNumber."' LIMIT 1";
            $parent_product_details = $db->fetchResult($sql);
            foreach($product_list->Components as $variations){
            $checkVariationsProducts = $db->getCount("SELECT count(*) c FROM  variations_products WHERE product_number='" . $variations->ProductNumber . "'");
            if(empty( $checkVariationsProducts)){
                $sql = "INSERT INTO `variations_products` (`id`, `variation_of`, `product_number`, `name`, `box_sizes`, `box_weight`, `cubes`, `quantity`) VALUES (NULL, '". $parent_product_details[0]['id']."',  '".$variations->ProductNumber."', '".addslashes($variations->Name)."', '".json_encode($variations->BoxSize)."', '".$variations->BoxWeight."', '".$variations->Cubes."', '".$variations->Qty."')";
                $db->executeQuery($sql);
            }else{
                $sql = "UPDATE `variations_products`  SET `name` = '".addslashes($variations->Name)."', `box_sizes` =  '".json_encode($variations->BoxSize)."',  `box_weight` =  '".$variations->BoxWeight."',  `cubes`= '".$variations->Cubes."',  `quantity` = '".$variations->Qty."'";
                $db->executeQuery($sql);
            }
            }
        }


    }
}
} 
die;